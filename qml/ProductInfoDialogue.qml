import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: aboutProductDialogue
    title: i18n.tr("Información del producto")
    text: "QML examples: versión " + root.appVersion + "<br/><br/>Ejemplos extraídos del libro <br/>Qt5 Cadaques Release master<br/>(Jürgen Bocklage-Ryannel<br/>y Johan Thelin)"

    Button {
        text: "Cerrar"
        onClicked: PopupUtils.close(aboutProductDialogue)
    }
}
