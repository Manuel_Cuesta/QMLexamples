/*
 * Copyright (C) 2020  Manuel Cuesta Herranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * qmlexamples is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'qmlexamples.manuel'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property string appVersion: "1.0"

    Component {
        id: productInfoDialog
        ProductInfoDialogue{}
    }

    Component {
        id: example_1
        Example_1{}
    }

    PageStack {
        id : pageStack

        // Establecer la primera página de la aplicación
        Component.onCompleted: {
            pageStack.push(mainPage)
        }

        // Página de inicio de la aplicación
        Page {
            id: mainPage
            anchors.fill: parent

            header: PageHeader {
                id: header
                title: i18n.tr('QML examples')
                StyleHints {
                    backgroundColor: "transparent"
                }

                // Acciones disponibles en trailingActionBar (la del lado derecho)
                trailingActionBar.actions: [
                    Action {
                        // Nota: 'iconName son nombres de archivos en /usr/share/icons/suru/actions/scalable
                        iconName: "info"
                        text: i18n.tr("Acerca de")
                        onTriggered: {
                            PopupUtils.open(productInfoDialog)
                        }
                    }
                ]
            }
            Column {
                anchors.top : header.bottom
                anchors.left : parent.left
                anchors.right : parent.right
                anchors.bottom : parent.bottom
                ListItem {
                   // Debemos especificar la altura cuando utilizamos ListItemLayout dentro de ListItem
                   height: layout.height + (divider.visible ? divider.height : 0)
                   ListItemLayout {
                       id: layout
                       title.text: "1. Meet Qt 5"
                   }
                   onClicked: pageStack.push(example_1)
                }
            } 
       }
   }
}
