import QtQuick 2.7
import Ubuntu.Components 1.3

Page {
    id: example_1

    header: PageHeader {
        id: header
        title: i18n.tr("Example 1")
    }

    Image {
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        //anchors.bottom: parent.bottom
        id: root
        source: "images/background.png"

        Image {
            id: pole
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            source: "images/pole.png"
        }

        Image {
            id: wheel
            Behavior on rotation {
                NumberAnimation {
                    duration: 250
                }
            }

            anchors.centerIn: parent
            source: "images/pinwheel.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: wheel.rotation += 90
        }
    }
    Label {
        id: explicacion
        anchors.top: root.bottom
        text: i18n.tr("Pulse en la imágen para hacer girar el molinillo")
        textSize: Label.Large
    }
}
